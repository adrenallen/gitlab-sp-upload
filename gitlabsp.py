import openpyxl
from io import BytesIO
from openpyxl import load_workbook
import re
import requests

#from openpyxl import load_workbook

def weighturl(iid, wght, pid=25):
	"Returns a URL to be PUT to for weight updates on a project issue"
	return "http://gitlab.com/api/v4/projects/" + str(pid) + "/issues/" + str(iid)

def noteurl(iid, pid=25):
	"URL for note to be posted to"
	return "http://gitlab.com/api/v4/projects/" + str(pid) + "/issues/" + str(iid) + "/notes"

def notemsg(total_sp, qa_sp, dev_sp):
	"the note message to post"
	return """ Setting ticket weight to """ + str(total_sp) + """

QA weight  -> """ + str(qa_sp) + """

Dev weight  -> """ + str(dev_sp)


url_col = "A"
point_col = "B"
qa_col = "C"
dev_col = "D"
row_num = 2
filename = 'weightcalcs.xlsx'
token = ''


wb = load_workbook(filename)
sheet_ranges = wb['Sheet1']

#looop the rows
#assuming first column is the issue URL and second is the weight
while True:
	if sheet_ranges[url_col+str(row_num)].value is None:
		print("Reached end, stopping")
		break
	m = re.search('.*issues\/(\d+)', sheet_ranges[url_col+str(row_num)].value)
	issue_num = m.group(1)

	

	#Make sure your sheet has these as values not formulas or it wont work
	sp = sheet_ranges[point_col+str(row_num)].value
	#qa_sp = sheet_ranges[qa_col+str(row_num)].value
	#dev_sp = sheet_ranges[dev_col+str(row_num)].value

	#print so we can see
	print(str(issue_num) + " updated to " + str(sp))
	print(weighturl(issue_num, sp))
	
	#updating total SP
	payload = {'private_token' : token, 'weight' : sp}
	requests.put(weighturl(issue_num, sp), data=payload)

	#adding note for dev and qa sp
	#payload = {'private_token' : token, 'body' : notemsg(sp, qa_sp, dev_sp)}
	#requests.post(noteurl(issue_num), data=payload)

	row_num += 1

quit()
